import UIKit

@objc(CardIOCordovaPlugin) class CardIOCordovaPlugin : CDVPlugin {


    var window: UIWindow?
    var vc = CardIOScreenTestViewController()


    @objc(scan:)
    func scan(command: CDVInvokedUrlCommand) {
        vc = CardIOScreenTestViewController()
         vc.topLabel.text = command.arguments[2] as? String
         vc.warnLabel.text = command.arguments[3] as? String
//        vc.attempts = command.arguments[2] as! Int32
//        vc.useName = command.arguments[3] as! Bool
//        vc.useNumber = command.arguments[4] as! Bool
        if command.arguments[1] as! String == "white" {
                vc.colorBackground = "#FFFFFF"
                }else{
                vc.colorBackground = "#1d3664"
                }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
        vc.segmentSelectionAtIndex = {[weak self] (image, number, monthyear, name) in
            
            self!.window = UIWindow(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            
            let base64String = image.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let message = ["cardImage": base64String, "cardNumber": number, "monthyear": monthyear, "cardholderName": name] as [AnyHashable: Any];
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: message);
            self!.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
        vc.backCallBack = {[weak self] in
            self!.window = UIWindow(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Back");
            self!.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
    }

    @objc(closeApp:)
    func closeApp(command: CDVInvokedUrlCommand) {
         exit(-1)
    }
}
